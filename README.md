# base

A base docker image to base all our Docker images from

## Debian vs Alpine

Both came in at similar image sizes after you install software required in most cases. Alpine was definitely smaller however Debian was impressively small, while still having good support for all weird and wonderful packages. 

Building some Alpine packages still required debian build helpers. 

It was determined that debian was a most efficient base for rapid development of containers. Alpine introduced time lost to resolving around musl. 

Alpine is fantastic. Debian is just cozier.
